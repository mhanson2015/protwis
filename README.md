PROTWIS
====

The next generation of GPCRdb and related systems.

Please read our [Wiki](https://bitbucket.org/gpcr/protwis/wiki/Home) for more information.

INSTALLATION
---

The recommended method of installation is using our Vagrant configuration.

Please follow the instructions [HERE][1].

[1]: https://bitbucket.org/gpcr/protwis_vagrant